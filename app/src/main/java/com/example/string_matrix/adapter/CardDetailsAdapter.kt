package com.example.string_matrix.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import com.example.string_matrix.databinding.ItemCardDetailBinding

class CardDetailAdapter(
) : RecyclerView.Adapter<CardDetailAdapter.CardDetailViewHolder>() {
    private var letters = mutableListOf<String>()

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = CardDetailViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: CardDetailViewHolder, position: Int) {
        val letter = letters[position]
        holder.loadLetter(letter)
    }

    fun addLetter(letters: List<String>) {
        this.letters = letters.toMutableList()
        notifyDataSetChanged()
    }

    override fun getItemCount() = letters.size

    class CardDetailViewHolder(
        private val binding: ItemCardDetailBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadLetter(letter: String) {
            binding.root.text = letter
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCardDetailBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CardDetailViewHolder(it) }
        }
    }
}
