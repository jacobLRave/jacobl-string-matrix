package com.example.string_matrix.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.string_matrix.databinding.ItemInputBinding

class InputAdapter(val nav: (String) -> Unit) :
    RecyclerView.Adapter<InputAdapter.InputViewHolder>() {

    private var words = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputViewHolder {
        val binding = ItemInputBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return InputViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InputViewHolder, position: Int) {
        val word = words[position]
        holder.loadColor(word)
        holder.navigateClick().setOnClickListener() {
            nav(word)
        }
    }

    override fun getItemCount(): Int {
        return words.size
    }

    fun addInput(words: List<String>) {

        this.words = words.toMutableList()
        notifyDataSetChanged()
    }

    class InputViewHolder(
        private val binding: ItemInputBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadColor(word: String) {
            binding.mvcContainer.text = word
        }

        fun navigateClick(): TextView {
            return binding.mvcContainer
        }
    }

}