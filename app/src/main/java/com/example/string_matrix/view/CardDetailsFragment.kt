package com.example.string_matrix.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.string_matrix.adapter.CardDetailAdapter
import com.example.string_matrix.databinding.FragmentCardDetailsBinding

class CardDetailsFragment : Fragment() {
    private var _binding: FragmentCardDetailsBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<CardDetailsFragmentArgs>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCardDetailsBinding.inflate(inflater, container, false).also {
        _binding = it

    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvColors.adapter =
            CardDetailAdapter().apply { addLetter(args.letter.map { it.toString() }) }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}