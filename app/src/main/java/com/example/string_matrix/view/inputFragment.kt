package com.example.string_matrix.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.string_matrix.adapter.InputAdapter
import com.example.string_matrix.databinding.FragmentInputBinding
import com.example.string_matrix.viewmodel.InputViewModel

class InputFragment : Fragment() {
    private var _binding: FragmentInputBinding? = null
    private val binding get() = _binding!!
    private val wordViewModel by viewModels<InputViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentInputBinding.inflate(inflater, container, false).also {
        _binding = it

    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnSubmit.setOnClickListener {
            with(binding.etSize) {
                var tempWordList =
                    if (wordViewModel._randomColor.value == null) mutableListOf<String>()
                    else wordViewModel._randomColor.value
                tempWordList?.add(text.toString())
                wordViewModel._randomColor.value = tempWordList
            }

        }
        wordViewModel.randomColor.observe(viewLifecycleOwner) { word ->
            binding.rvList.apply {
                layoutManager = GridLayoutManager(context, 3)
                adapter = InputAdapter(::navigate).apply {
                    addInput(word)

                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun navigate(word: String) {
        findNavController().navigate(
            InputFragmentDirections.actionInputFragmentToCardDetailsFragment2(word)
        )
    }
}