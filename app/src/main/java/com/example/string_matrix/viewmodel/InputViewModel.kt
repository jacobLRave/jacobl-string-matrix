package com.example.string_matrix.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class InputViewModel : ViewModel() {
    var _randomColor = MutableLiveData<MutableList<String>>()
    val randomColor: LiveData<MutableList<String>> get() = _randomColor

}